package com.example.kyle.molarmassapp;

public class Element
{
	private String name,symbol;
	private int atomicNum;
	private double atomicMass;
	private int sigFigs;
	/**
	 *
	 * @param atomicNum
	 * @param symbol
	 * @param name
	 * @param mass
	 * @param sigFigs
	 */
	public Element(String atomicNum, String symbol, String name,
				   String mass, String sigFigs) {
		super();
		this.name = name.substring(0,name.length()-1);
		this.atomicMass=Double.parseDouble(mass.substring(0,mass.length()-1));
		this.atomicNum =Integer.parseInt(atomicNum.substring(0,atomicNum.length()-1));
		this.symbol = symbol.substring(0,symbol.length()-1);
		this.sigFigs=Integer.parseInt(sigFigs);
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Element [name=" + name + ", symbol=" + symbol + ", atomicNum="
				+ atomicNum + ", atomicMass=" + atomicMass + ", sigFigs="
				+ sigFigs + "]";
	}
	/**
	 * @return the name
	 */
	protected String getName() {
		return name;
	}
	/**
	 * @return the atomicNumm
	 */
	protected int getAtomicNum() {
		return atomicNum;
	}
	/**
	 * @return the symbol
	 */
	protected String getSymbol() {
		return symbol;
	}
	/**
	 * @return the atomicMass
	 */
	protected double getAtomicMass() {
		return atomicMass;
	}

	/**
	 * @return the sigFigs
	 */
	protected int getSigFigs() {
		return sigFigs;
	}

}
