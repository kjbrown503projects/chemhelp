package com.example.kyle.molarmassapp;

		import android.content.Context;

        import java.io.DataInputStream;
		import java.io.File;
		import java.io.FileNotFoundException;
		import java.util.HashMap;
		import java.util.Scanner;



public class GrabData
{
    Context myContext;

    public GrabData(Context myContext)
    {
        this.myContext=myContext;
    }

	public HashMap grabData()
	{
		Element temp;
		HashMap<String, Element> chemicals=new HashMap<String, Element>();
		Scanner input=null;
		{
			try
			{

				DataInputStream dInStream = new DataInputStream(myContext.getAssets().open(String.format("simpleTable.csv")));
				input = new Scanner(dInStream);
				while(input.hasNext())
			    {
			        temp=new Element(input.next(),input.next(),input.next(),input.next(),input.next());
				    chemicals.put(temp.getSymbol(), temp);
			    }

				input.close();
			}
			catch (java.io.IOException e)
			{
				System.out.println("Periodic table file missing.");
				e.printStackTrace();
			}
		}
		return chemicals;
	}
}
