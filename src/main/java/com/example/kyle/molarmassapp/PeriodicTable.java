package  com.example.kyle.molarmassapp;

import android.content.Context;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.HashMap;

public class PeriodicTable {
	private HashMap<String, Element> chemicals;
	private double molarMass = 0;
	private int sigFig=0;
	ArrayList <String[]> elementInfo;

	public PeriodicTable(Context matchContext) {
		super();
		GrabData parse = new GrabData(matchContext);
		chemicals = parse.grabData();
	}

	/**
	 * calculates molar mass from molecule given
	 *
	 * @param molecule
	 * @return Molar mass w/o sig fig correction
	 */

	public double molarMassCalc(String molecule)
	{
		elementInfo = this.breakApart(molecule);
		this.setSigFig();
		double atomicMass;
		for (int i = 0; i < elementInfo.size(); i++)
		{
			//population works fine	cannot fetch from hashMap

			atomicMass=((Element) chemicals.get(elementInfo.get(i)[0])).getAtomicMass();
//			this.sigFigCorrection(atomicMass);
//			elementInfo[i][2]=String.valueOf(sigFigCorrection(((Element) chemicals.get(elementInfo[i][0])).getAtomicMass()
//					* Integer.parseInt(elementInfo[i][1])));

			elementInfo.get(i)[2]=String.valueOf(((Element) chemicals.get(elementInfo.get(i)[0])).getAtomicMass()
					* Integer.parseInt(elementInfo.get(i)[1]));
			molarMass+=Double.parseDouble(elementInfo.get(i)[2]);
			System.out.println(molarMass);
		}
		return molarMass;
	}

	/**
	 * Breaks apart molecule given by user into 2 by x
	 * dimensional String array where x is the number of chemicals
	 *
	 * @param molecule
	 * @return String [][] in which first column is chemical symbol and second
	 *         is how many of that element there are
	 */
	private ArrayList<String[]> breakApart(String molecule) {
		ArrayList symbolNum=new ArrayList<String[]>();
		String[] piece = new String[3];
		String temp = molecule + ".";
		int index = 0;
		boolean capital;
		// 65-90 ascii for capital char
		// 48-57 for numbers
		// period is 46

		while (!temp.equals(".")) {
			String amount = "0";
			String current = "" + temp.charAt(0);
			temp = temp.substring(1);
			capital = false;
			// either number,lowercase with number or capital
			while (!capital) {
				if (((int) temp.charAt(0) > 90 || (int) temp.charAt(0) < 58)
						&& !(temp.charAt(0) == '.'))// if lowerCase or number
				{
					if ((int) temp.charAt(0) > 90)// if lowerCase
					{
						current += temp.charAt(0);
						temp = temp.substring(1);
					} else if ((int) temp.charAt(0) < 58)// if number
					{
						amount = ""
								+ ((Integer.parseInt(amount) * 10) + (temp
								.charAt(0) - 48));
						temp = temp.substring(1);
					} else
						capital = true;
				} else
					capital = true;
			}
			if (!amount.equals("0"))
				piece = new String[] { current, amount,""};
			else
				piece = new String[] { current, "1",""};
			index++;
			symbolNum.add(piece);

		}

		return symbolNum;
	}

	public void percentAbundance()
	{
		sigFig=6;
		for(int i=0;i<elementInfo.size();i++)
		{
			double abundance=100.0*(Double.parseDouble(elementInfo.get(i)[2])/molarMass);
			System.out.println(elementInfo.get(i)[0]+": "+this.sigFigCorrection(abundance)+"%");

		}
		System.out.println("sf"+sigFig);
	}
	/**
	 * corrects for Significant figures
	 * @param rawDecimal
	 * @return
	 */
	private double sigFigCorrection(double rawDecimal)
	{
		BigDecimal sF = new BigDecimal(rawDecimal);
		sF = sF.round(new MathContext(sigFig));
		return sF.doubleValue();
	}

	private void setSigFig()
	{
		sigFig=chemicals.get(elementInfo.get(0)[0]).getAtomicNum();
		for(int i=1;i<elementInfo.size();i++)
		{
			if(chemicals.get(elementInfo.get(i)[0]).getAtomicNum()>sigFig);
			sigFig=chemicals.get(elementInfo.get(i)[0]).getAtomicNum();
		}
	}

}
